﻿
#include <iostream>
#include <cmath>
class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(int newX, int newY, int newZ): x(newX), y(newY), z(newZ)
	{}
	void Coord()
	{
		std::cout << "X = " << x << "\nY = " << y << "\nZ = " << z<<"\n";
	}
	float Leng()
	{
		return (sqrt(x * x + y * y + z * z));
	}
private:
	float x=0, y=0, z=0;
};


int main()
{
	Vector v(1,1,1);
	v.Coord();
	std::cout<<v.Leng();
}
